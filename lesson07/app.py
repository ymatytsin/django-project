import math

class Shape:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self):
        pass

    def get_square(self):
        raise NotImplementedError()

    def get_center(self):
        return self.x, self.y

    def __str__(self):
        return f'Shape with center {self.x} {self.y}'


# ---------------------------------------------------------

class Circle(Shape):
    def __init__(self, x, y, r):
        super().__init__(x, y)
        self.r = r

    def get_square(self):
        s = math.pi * (self.r**2)
        return  s

    def __str__(self):
        return f'Circle with center {self.x} {self.y} and radius {self.r}'


# ---------------------------------------------------------

class EquilateralTriangle(Shape):
    # Треугольник у которого все стороны равны
    def __init__(self, x, y, a):
        super().__init__(x, y)
        self.a = a

    def get_square(self):
        s = ((self.a**2) * math.sqrt(3)) / 4
        return s

    def __str__(self):
        return f'EquilateralTriangle with center {self.x} {self.y} and length of all sides {self.a}'


# ---------------------------------------------------------

class Rectangle(Shape):
    def __init__(self, x, y, a, b):
        super().__init__(x, y)
        self.a = a
        self.b = b

    def get_square(self):
        return self.a*self.b

    def __str__(self):
        return f'Rectangle with center {self.x} {self.y} and length of all side {self.a} and {self.b}'


# ---------------------------------------------------------

class ShapeContainer:
    def __init__(self):
        self.lst = []

    def append(self, shape):
        self.lst.append(shape)

    def pop(self):
        return self.lst.pop()

    def size(self):
        return len(self.lst)

    def get_square(self):
        square = 0
        for i in self.lst:

            if type(i) == Circle:
                square += i.get_square()
            if type(i) == EquilateralTriangle:
                square += i.get_square()
            if type(i) == Rectangle:
                square += i.get_square()

        return f'the area of all figures is equal to: {square}'

# ---------------------------------------------------------

####  the second task
c1 = Circle(1, 2, 3)
r1 = Rectangle(1,4,1,2)

shapes = ShapeContainer()
shapes.append(c1)
shapes.append(r1)

print(shapes.get_square())


#####  the first task
# et1 = EquilateralTriangle(1,2,3)
# print(et1.get_square())
#
# et2 = Circle(1,2,13)
# print(et2.get_square())
#
# et3 = Rectangle(1,2,33, 7)
# print(et3.get_square())




