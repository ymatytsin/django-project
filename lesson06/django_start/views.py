
import string
from django.http import HttpResponse

from lesson06.django_start.db import execute_query
from lesson06.django_start.utils import generate_passwords
from lesson06.django_start.validation import get_integer_argument


def index(request):
    return HttpResponse('Hello')


def get_random(request):
    try:
        length = get_integer_argument(request, 'length', '10', 5, 100)
        count = get_integer_argument(request, 'count', '1', 1, 20)
    except ValueError as e:
        return HttpResponse(str(e), status=400)

    chars = string.ascii_lowercase + string.ascii_uppercase

    passwords = generate_passwords(
        chars,
        length,
        count)

    response = HttpResponse("\n".join(passwords))
    response.headers['Content-Type'] = 'text/plain'
    return response


def get_customers_by_country(request):
    country = request.GET['country']
    result = execute_query("""SELECT FirstName, LastName FROM Customers 
                            WHERE Country=?""", (country, ))
    result_list = []
    for item in result:
        result_list.append(str(item))

    response = HttpResponse("\n".join(result_list))
    response.headers['Content-Type'] = 'text/plain'
    return  response


def get_sales(request):
    result = execute_query("SELECT SUM(UnitPrice * Quantity) FROM Invoice_Items")
    response = HttpResponse(result[0], content_type="text/plain")

    return  response


def get_genres(request):
    result = execute_query("""
        SELECT genres.name, sum(Milliseconds)/1000 AS seconds 
        FROM genres
        LEFT JOIN tracks ON tracks.GenreId = genres.GenreId
        GROUP BY genres.Name
    """)

    result_list = []
    for item in result:
        y = (f'{str(item[0])}, {str(item[1])}')
        result_list.append(y)

    response = HttpResponse("\n".join(result_list), content_type="text/plain")
    return  response