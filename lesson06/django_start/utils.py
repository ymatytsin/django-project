import os
import random

filename = os.path.join(
    os.path.dirname(__file__),
    'requirements.txt'
)

print(filename)

# with open(filename, 'r', encoding='utf-8') as f:
#     print(f.read())


def generate_passwords(symbols, length, count):
    passwords = []
    for iteration in range(count):
        result = []
        for i in range(length):
            result.append(random.choice(symbols))
        passwords.append("".join(result))
    return passwords
