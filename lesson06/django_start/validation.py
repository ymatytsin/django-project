
def get_integer_argument(request, name, default_value,
                         min_value=float('-inf'),
                         max_value=float('inf')):
    value = request.GET.get(name, default_value)
    try:
        value = int(value)
    except ValueError:
        raise ValueError(f"Error. {name} is not integer")

    if value < min_value:
        raise ValueError(f"Error. {name} less than {min_value}")

    if value > max_value:
        raise ValueError(f"Error. {name} more than {max_value}")

    return value
