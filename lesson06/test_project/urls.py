"""test_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from lesson06.django_start.views \
    import index, get_random, get_customers_by_country, get_sales, get_genres

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', index),
    path('random/', get_random),
    path('get_customers_by_country/', get_customers_by_country),
    path('sales/', get_sales),
    path('genres/', get_genres)
]
